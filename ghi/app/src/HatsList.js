import { React, useState, useEffect } from "react";

function HatsList() {

  const [hatList, setHatList] = useState([]);

	const fetchData = async () => {
		const url = 'http://localhost:8090/api/hats/';
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setHatList(data);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

    const handleDelete = async(id) => {
        const response = await fetch(`http://localhost:8090/api/hats/${id}/`, {
            method: 'delete',
        });
        if (response.ok) {
            fetchData();
        }
    }


    return(
      <table className="table table-hover table-striped border border-5">
        <thead>
          <tr>
            <th>Style</th>
            <th>Color</th>
            <th>Fabric</th>
            <th>Location</th>
            <th>Image</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {hatList.hats?.map(hat => {
            return (
              <tr key={ hat.id }>
                <td class='text-center'>{ hat.style_name }</td>
                <td class='text-center'>{ hat.color }</td>
                <td class='text-center'>{ hat.fabric }</td>
                <td class='text-center'>{ hat.location}</td>
                <td><img src={hat.picture_url} width="100" height="100" />{}</td>
                <td>
                    <button type="button" className="btn btn-light" onClick={() => handleDelete(hat.id)}>Delete</button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )
}

export default HatsList;